var movies = [
        {
            id: 0,
            name: "Abraham Lincoln: Vampire Hunter",
            synopsis: "President Lincoln's mother is killed by a supernatural creature, which fuels his passion to crush vampires and their slave-owning helpers. The secret life of our nation's favorite president...as history's greatest hunter of the undead.",
            director: "Timur Bekmambetov",
            starring: "Benjamin Walker, Dominic Cooper, and Anthony Mackie",
            image: "lincoln.jpg",
            score: 42
        },

        {
            id: 1,
            name: "The Hunger Games",
            synopsis: "Set in a future where the Capitol selects a boy and girl from the twelve districts to fight to the death on live television, Katniss Everdeen volunteers to take her younger sister's place for the latest match.",
            director: "Gary Ross",
            starring: "Jennifer Lawrence, Josh Hutcherson, and Liam Hemsworth",
            image: "hunger_games.jpg",
            score: 44
        },

        {
            id: 2,
            name: "The Amazing Spider-Man",
            synopsis: "Peter Parker finds a clue that might help him understand why his parents disappeared when he was young. His path puts him on a collision course with Dr. Curt Connors, his father's former partner.",
            director: "Marc Webb",
            starring: "Andrew Garfield, Emma Stone, and Rhys Ifans",
            image: "spiderman.jpg",
            score: 47
        },

        {
            id: 3,
            name: "Brave",
            synopsis: "Determined to make her own path in life, Princess Merida defies a custom that brings chaos to her kingdom. Granted one wish, Merida must rely on her bravery and her archery skills to undo a beastly curse.",
            director: "Mark Andrews, Brenda Chapman, and Steve Purcell",
            starring: "Kelly McDonald, Billy Connolly, and Emma Thompson",
            image: "brave.jpg",
            score: 69
        },

        {
            id: 4,    
            name: "The Avengers",
            synopsis: "Nick Fury of S.H.I.E.L.D. brings together a team of super humans to form The Avengers to help save the Earth from the supervillain Loki and his army of extraterrestrials.",
            director: "Joss Whedon",
            starring: "Robert Downey Jr., Chris Evans, and Scarlett Johansson",
            image: "avengers.jpg",
            score: 43
        },

        {
            id: 5,
            name: "Prometheus",
            synopsis: "A team of explorers discover a clue to the origins of mankind on Earth, leading them on a journey to the darkest corners of the universe. There, they must fight a terrifying battle to save the future of the human race.",
            director: "Ridley Scott",
            starring: "Noomi Rapace, Logan Marshall-Green, and Michael Fassbender",
            image: "prometheus.jpg",
            score: 41
        }
    ];

exports.movies = movies;
