YUI({
        gallery: 'gallery-2012.07.11-21-38', 
        filter: 'raw',
        modules: {
            'socket-io': {
                fullpath: '/socket.io/socket.io.js'
            },
            'md5': {
                fullpath: '/js/md5.js'
            },
            'spin': {
                fullpath: '/js/spin.js'
            }
        },
    }).use('sortable', 'app', 'model', 'model-list', 'handlebars', 'gallery-model-sync-socket', 'socket-io', 'cookie', 'md5', 'spin', function (Y) {
    var Signal, Product, ProductList, User, UserList, SwankApp;
   
    Signal = Y.Base.create('signal', Y.Model, [Y.ModelSync.Socket], {
        root: '/signals'
    }, {
        ATTRS: {
            id: '',
            message: ''
        }
    });

    Product = Y.Base.create('product', Y.Model, [Y.ModelSync.Socket], {
        root: '/products'
    }, {
        ATTRS: {
            id: '',
            name: '',
            synopsis: '',
            director: '',
            starring: '',
            score: 0
        }
    });

    ProductList = Y.Base.create('productList', Y.ModelList, [Y.ModelSync.Socket], {
        model: Product,
        url: '/products'
    });

    User = Y.Base.create('user', Y.Model, [Y.ModelSync.Socket], {
        root: '/users'
    }, {
        ATTRS: {
            id: '',
            name: '',
            email: '',
            ranking: []
        }
    });

    UserList = Y.Base.create('userList', Y.ModelList, [Y.ModelSync.Socket], {
        model: User,
        url: '/users'
    });


    SwankApp = Y.Base.create('swankApp', Y.App, [], {
        containerTemplate: '#main',

        iconTemplate: Y.Handlebars.compile(Y.one('#icon-template').getHTML()),

        productTemplate: Y.Handlebars.compile(Y.one('#product-template').getHTML()),

        streamTemplate: Y.one('#stream-template').getHTML(),

        predictTemplate: Y.Handlebars.compile(Y.one('#prediction-template').getHTML()),

        winnerTemplate: Y.Handlebars.compile(Y.one('#winner-template').getHTML()),
        
        initializer: function () {
            this.set('products', new ProductList());
            this.set('user', new User({
                name: Y.Cookie.get('name'), 
                email: Y.Cookie.get('email')
            }));
            this.set('users', new UserList());
            this.set('signal', new Signal({id: 9001}));

            var products = this.get('products'),
                user     = this.get('user'),
                users    = this.get('users'),
                signal   = this.get('signal'),
                self     = this;

            products.onSocket('read', function (e) {
                products.add(e.data);
            });

            users.onSocket('construct', function (e) {
                users.add(e.data);
            });

            signal.onSocket('update', this.endGame, this);

            products.after('add', this.addProduct, this);
            users.after('add', this.addUser, this);
            
            products.load();

            Y.one('.product-top').setHTML(this.productTemplate({
                id: 3,
                name: "Brave",
                synopsis: "Determined to make her own path in life, Princess Merida defies a custom that brings chaos to her kingdom. Granted one wish, Merida must rely on her bravery and her archery skills to undo a beastly curse.",
                director: "Mark Andrews, Brenda Chapman, and Steve Purcell",
                starring: "Kelly McDonald, Billy Connolly, and Emma Thompson",
                image: "brave.jpg",
                score: 69
            }));

            Y.one('.product-bottom').setHTML(this.streamTemplate);

            Y.one('#submit-rank').on('click', this.submitData, this);

            if (window.location.href.indexOf('master') != -1) {
                window.setTimeout(function () {
                    var signal = self.get('signal');
                    signal.set('message', 'STOP');
                    signal.save();
                }, 45000);
            }
        },

        addProduct: function (e) {
            var model = e.model,
                self  = this;

            this.get('ribbon').appendChild(self.iconTemplate({
                image: model.get('image'),
                name: model.get('name')
            }));
        },

        addUser: function (e) {
            var model        = e.model,
                existingNode = Y.one('#prediction-list p'),
                self         = this;

            if (existingNode) {
                existingNode.remove();
                Y.one('#prediction-list img').remove();
            }

            Y.one('#prediction-list').appendChild(self.predictTemplate({
                email: model.get('email'),
                name: model.get('name')
            }));
        },

        getProductOrder: function () {
            var ribbon   = this.get('ribbon'),
                products = ribbon.all('.img-sort');
                results  = [];

            products.each(function (node) {
                results.push(node.get('alt')); 
            });

            return results;
        },

        submitData: function (e) {
            Y.one('.new-btn').detach('click');
            Y.one('.new-btn').setHTML('Thank you! Please wait!');
            var user = this.get('user'),
                products = this.get('products');
            user.save();

            products.map(function (model) {
                model.set('complete', true);
            });
        },

        endGame: function (e) {
            Y.one('.ribbon-sortable').remove();
            Y.one('.product-top').empty();
            Y.one('.product-bottom').empty();
            Y.one('.ribbon-front').appendChild('<h1 id="welcome-title">Thank you very much for playing!</h1>');
            Y.one('.ribbon-front').appendChild('<h2 id="welcome-sub">Our winners are:</h1>');
            var users = this.get('users');
            var correct = ['Brave', 'The Amazing Spider-Man', 'The Hunger Games', 'The Avengers', 'Abraham Lincoln: Vampire Hunter', 'Prometheus'];
            var winners = users.filter(function (model) {
                var ranking = model.get('ranking');
                /* for (var i = 0; i < correct.length; i++) {
                    if (ranking[i] !== correct[i]) {
                        return false;
                    }
                } */
                return true;
            });
            var finalResult = Y.one('.product-top');
            for (var j = 0; j < winners.length; j++) {
                finalResult.appendChild(this.winnerTemplate({
                    email: winners[j].get('email'),
                    name: winners[j].get('name')
                }));
            }
        }
    }, {
        ATTRS: {
            ribbon: {
                valueFn: function () {
                    return Y.one('.ribbon-sortable');
                }
            },
            
            productTop: {
                valueFn: function () {
                    return Y.one('.product-top');
                }
            }
        }
    });

    var startApplication = function () {
        Y.one('#welcome-title').remove();
        var app = new SwankApp();

        window.setTimeout(function () {
        
            app.get('user').set('ranking', app.getProductOrder());
    
            var sortable = new Y.Sortable({
                container: '.ribbon-sortable',
                nodes    : 'img',
                opacity  : '0.1'
            });
            
            sortable.delegate.after('drag:start', function (e) {
                var node       = sortable.delegate.get('currentNode'),
                    name       = node.get('alt'),
                    products   = app.get('products'),
                    productTop = app.get('productTop');
            
                products.map(function (model) {
                    if (model.get('name') === name) {
                        productTop.empty();
                        productTop.appendChild(app.productTemplate(model.toJSON()));
                    }
                 });
        
                Y.one('#submit-rank').on('click', app.submitData, app);
            });

            sortable.delegate.after('drag:end', function (e) {
                var clientResults = app.getProductOrder(),
                    user          = app.get('user');

                user.set('ranking', clientResults);
            });
        }, 250);
    }

    var startWaitQueue = function () {
        var name  = Y.Escape.html(Y.one('.namefield').get('value')),
            email = Y.Escape.html(hex_md5(Y.one('.emailfield').get('value')));

        Y.Cookie.set('name', name);
        Y.Cookie.set('email', email);

        Y.one('.product-top').empty();
        Y.one('#welcome-title').setHTML("Waiting for next available round...");
        Y.one('#welcome-sub').remove();
        Y.one('.product-top').set('id', 'spinner-container');
        var opts = {
            lines: 17, // The number of lines to draw
            length: 22, // The length of each line
            width: 4, // The line thickness
            radius: 33, // The radius of the inner circle
            rotate: 0, // The rotation offset
            color: '#000', // #rgb or #rrggbb
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px
        };
        var target = document.getElementById('spinner-container');
        var spinner = new Spinner(opts).spin(target);

        var signal = new Signal({id: 1337});
        signal.onSocket('update', function (e) {
            spinner.stop();
            Y.one('.product-top').set('id', '');
            startApplication(); 
        });
        if (window.location.href.indexOf('master') != -1) {
            signal.set('message', 'GO');
            signal.save();
        }
    }
    
    window.socket = io.connect('http://184.72.60.151/');
    Y.one('#signup-btn').on('click', startWaitQueue);
    Y.one('.namefield').on('keypress', function (e) {
        if (e.keyCode === 13) {
            startWaitQueue();
        }
    });
    Y.one('.emailfield').on('keypress', function (e) {
        if (e.keyCode === 13) {
            startWaitQueue();
        }
    });
});
